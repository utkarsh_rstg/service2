FROM openjdk:8
EXPOSE 8081
ADD target/centime-service2.jar centime-service2.jar
ENTRYPOINT ["java","-jar","/centime-service2.jar"]