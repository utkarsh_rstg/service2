package com.assignment.utkarsh.service2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceController {

	@Value("${greetingMessage}")
	private String message;
	
	private static final Logger logger = LoggerFactory.getLogger(ServiceController.class);

	@GetMapping("/*")
	public ResponseEntity<String> returnGreeting() {
		logger.info("Get Call Received to Service 2");
		logger.info("Greeting Message returned ->" +message);
		return new ResponseEntity<String>(message, HttpStatus.OK);
	}

}
